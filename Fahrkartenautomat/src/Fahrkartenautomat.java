﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag;				//Der zu zahlende Betrag, ein double, da mit bruchteilen von Euros gerechnet wird, wird mit der Anzahl der Fahrkarten multipliziert, der gezahlte Betrag wird von ihm abgezogen
       double eingezahlterGesamtbetrag;			//Der Betrag, der bereits eingezahlt wurde, ein double, da mit bruchteilen von Euros gerechnet wird, wird vom zu zahlenden Betrag abgezogen 
       double eingeworfeneMünze;				//Der Wert der gerade eingeworfenen Muenze, ein double, da mit bruchteilen von Euros gerechnet wird, wird von dem restlichen zu zahlenden Betrag abgezogen
       double rückgabebetrag;					// Der Betrag, der zurueck gegeben werden muss, ein double, da mit bruchteilen von Euros gerechnet wird, der Wert der eingeworfenen Muenze wird von ihm abgezogen
       int anzahlKarten;						// Die Anzahl der gewuenschten Karten, ein int, da keine Bruchstuecke von Karten verkauft werden koennen, wird mit dem Einzelfahrpreis multipliziert 
       
       
       // Fahrpreisberechnung
       // -----------
       zuZahlenderBetrag = Fahrpreis(tastatur);

       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = Geldeinwurf(zuZahlenderBetrag, tastatur);
       
       // Fahrscheinausgabe
       // -----------------
       Fahrscheinausgabe();

       // Rückgeldberechnung und -Ausgabe
       // -------------------------------
       Rueckgeld(eingezahlterGesamtbetrag, zuZahlenderBetrag);
    }
    
    public static double Fahrpreis(Scanner tastatur)
    {
    System.out.print("Einzelfahrpreis : ");
    double zuZahlenderBetrag = tastatur.nextDouble();
    //Abfrage Anzahl Fahrkarten und Berechnung Gesamtpreis
    System.out.println("Gewuenschte Anzahl der Fahrkarten:");
    int anzahlKarten = tastatur.nextInt();
    zuZahlenderBetrag = (double)anzahlKarten * zuZahlenderBetrag;
    //Bei der obigen Berechnung wird der integer in einen double umgewandelt um eine fehlerfreie Berechnung moeglich zu machen
    return zuZahlenderBetrag;
    }
    
    public static double Geldeinwurf(double zuZahlenderBetrag, Scanner tastatur)
    {
    double eingeworfeneMünze;
    double eingezahlterGesamtbetrag = 0.0;
    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
    {
 	   System.out.print("Noch zu zahlen: ");
 	   System.out.printf("%.2f Euro %n",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
 	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 	   eingeworfeneMünze = tastatur.nextDouble();
        eingezahlterGesamtbetrag += eingeworfeneMünze;
    }
    return eingezahlterGesamtbetrag;
    }

    public static void Fahrscheinausgabe()
    {
    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++)
    {
       System.out.print("=");
       try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    System.out.println("\n\n");
    }
    
    public static void Rueckgeld(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
    {
    double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        }
    }
    
    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
    }
}