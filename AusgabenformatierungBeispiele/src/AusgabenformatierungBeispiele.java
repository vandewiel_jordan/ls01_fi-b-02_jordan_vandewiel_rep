
public class AusgabenformatierungBeispiele {

	public static void main(String[] args) {
		
		// string formatting
/*		System.out.printf("|%s|%n", "123456789");
		System.out.printf("|%20s|\n", "123456789");
		System.out.printf("|%-20s|\n", "123456789");
		System.out.printf("|%20.3s|\n", "123456789");
*/		
		// int formatting
/*		System.out.printf("|%d|%n",123456789);
		System.out.printf("|%20d|%n",123456789);
		System.out.printf("|%-20d|%n",123456789);
		System.out.printf("|%-20d|%n",-123456789);
		System.out.printf("|%+-20d|%n",123456789);
		System.out.printf("|%020d|%n",-123456789);
*/
		// float formatting
		System.out.printf("|%f|%n",123456.789);
		System.out.printf("|%.2f|%n",123456.789);
		System.out.printf("|%20.2f|%n",123456.789);
		System.out.printf("|%-20.2f|%n",123456.789);
		System.out.printf("|%020.2f|%n",123456.789);
		System.out.printf("|%+020f|%n",123456.789);


	}

}